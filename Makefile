CXX			= g++
SOURCES = bounded_queue.cpp unit_test-1.cpp
OBJECTS = $(wildcard *.o)
EXECUTABLE = bounded_queue_testing
CXXFLAGS = -std=c++11 -Wall -Werror -Wextra -Wvla -pedantic -O3
SUBMIT_FILE = submit.tar.gz

# Compile the class and test cases.
release:
	$(CXX) $(CXXFLAGS) $(SOURCES) -o $(EXECUTABLE)
	./$(EXECUTABLE)

# make clean - remove executable and tarball
clean:
	rm -f $(EXECUTABLE) $(OBJECTS) $(SUBMIT_FILE)

# Compress code into a tarball
MY_FILES=$(wildcard Makefile README *h *cpp unit_test*.txt)
$(SUBMIT_FILE): $(MY_FILES)
	tar -vvczf $(SUBMIT_FILE) $(MY_FILES)

submit: submit.tar.gz
