/* bounded_queue.cpp
 *
 * by Steven Godbold
 * steven.godbold@gmail.com
 */

#include "bounded_queue.h"
#include <iostream>

// Effects: Initializes a queue with the given size.
bounded_queue::bounded_queue(const int size_in) {
  size = size_in;
  arr = new int[size];
  push_ptr = arr;
  pop_ptr = arr;
}

bounded_queue::~bounded_queue() {
  delete[] arr;
  arr = push_ptr = pop_ptr = nullptr;
}

// Effects: Push elt onto the back of the queue if there's room left.
// Time Complexity: O(1)
void bounded_queue::enqueue(const int elt) {
  if(elements == size) return;

  *push_ptr = elt;
  push_ptr == arr + size-1 ? push_ptr = arr : push_ptr++;
  elements++;
  return;
}

// Effects: Returns and pops the front element of the queue.
//          Returns 1 if empty. Be sure to check if it's empty first.
// Time Complexity: O(1)
int bounded_queue::dequeue() {
  if(is_empty()) return 0;

  int num = *pop_ptr;
  pop_ptr == arr + size-1 ? pop_ptr = arr : pop_ptr++;
  elements--;
  return num;
}
