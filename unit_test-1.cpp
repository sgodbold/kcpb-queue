/* unit_test-1.cpp
 *
 * Tests the queue by pushing and popping random numbers.
 *
 * by Steven Godbold
 * steven.godbold@gmail.com
 */

#include "bounded_queue.h"
#include <iostream>
#include <stdlib.h> // srand, rand
#include <assert.h>

using namespace std;

int main() {
  const int SIZE = 10;
  int rand_num[SIZE];
  int num_dequeued;
  bounded_queue q1(SIZE);

  // initialize seed
  srand(time(NULL));

  // enqueing
  cout << "Enqueing random integers:";
  for(int i=0; i < SIZE; i++) {
    rand_num[i] = rand() % 1000 + 1; // random number in range 1 to 100
    cout << " " << rand_num[i] << " ";
    q1.enqueue(rand_num[i]);
  }
  cout << endl;

  // dequeing
  cout << "Dequing:\n";
  for(int i=0; i < SIZE; i++) {
    num_dequeued = q1.dequeue();
    cout << num_dequeued << endl;
    assert(num_dequeued == rand_num[i]); // Halt if they are not equal.
  }
  cout << endl;

  cout << "Passed Unit Test\n";

  return 0;
}
