/* bounded_queue.h
 *
 * by Steven Godbold
 * steven.godbold@gmail.com
 */

#ifndef QUEUE_H
#define QUEUE_H

class bounded_queue {
  public:
    bounded_queue(const int size);
    ~bounded_queue();
    void enqueue(const int elt);
    int dequeue();
    bool is_empty() { return elements == 0; };

  private:
    int size = 0;
    int elements = 0;
    int* arr;
    int* push_ptr; // points to the back of the queue
    int* pop_ptr; // points to the front of the queue
};

#endif
